export interface CorreoModel {
    nombre?: string;
    email?: string;
    asunto?: string;
    telefono?: number;
    mensaje?: string;
}