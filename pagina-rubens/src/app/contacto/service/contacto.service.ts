import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CorreoModel } from './correo.model';

@Injectable({providedIn: 'root'})
export class ContactoService {
    public URL_ENVIAR_CORREO_API = 'https://app-enviar-correo.herokuapp.com/envia-mail';

    constructor(private httpClient: HttpClient) { }
    public enviarCorreoService(correo: CorreoModel): Promise <any> {

        return this.httpClient.post(this.URL_ENVIAR_CORREO_API, correo).toPromise();
    }
}
