import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContactoService } from './service/contacto.service';
import { CorreoModel } from './service/correo.model';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {

  public correo: CorreoModel = {};
  public correoForm: FormGroup;




  constructor(private fb: FormBuilder, private contactoService: ContactoService, private toastr: ToastrService){

   }

  ngOnInit(): void {

    this.correoForm = this.fb.group({

      controlNombre: ['', Validators.compose([Validators.required, Validators.pattern('^[\\s\\S]{1,100}$')])],
      controlEmail: ['', Validators.compose([Validators.required, Validators.email])],
      controlAsunto: ['', Validators.compose([Validators.required, Validators.pattern('^[\\s\\S]{1,200}$')])],
      controlTelefono: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{1,10}$')])],
      controlMensaje: ['', Validators.compose([Validators.pattern('^[\\s\\S]{1,500}$')])],

    });
  }

  enviarCorreo(): void {

    if (this.correoForm.valid === false) {
      Object.keys(this.correoForm.controls).forEach(field => {
        const control = this.correoForm.get(field);
        if (control instanceof FormControl) {
          control.markAsTouched({onlySelf: true});
        }
      });
      this.toastr.warning('Se deben llenar todos los campos');
      return;
    }

    console.log(this.correo);
    this.contactoService.enviarCorreoService(this.correo).then(respuesta => {
      this.toastr.success('El correo se envió correctamente, espere respuesta.');
      console.log(respuesta);
    }).catch(respuesta => {
        console.log(respuesta);
        this.toastr.error('El correo no se pudo enviar, intente más tarde');
    });
  }

}



